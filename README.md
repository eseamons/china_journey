#Database:

china_journey_db



tables

user
  user_id
  firstname
  lastname
  phone_number
  email
  address
  

permissions
  permission_id
  user_id
  permission_type
  
  
Queries

CREATE TABLE users (user_id INT AUTO_INCREMENT, username varchar(40), password varchar(40), firstname varchar(40) NOT NULL, lastname varchar(40) NOT NULL, email varchar(40) NOT NULL UNIQUE, phone_number varchar(40) NOT NULL, address varchar(60) NOT NULL, PRIMARY KEY (user_id));

INSERT INTO users (username,password,firstname,lastname,email,phone_number,address) VALUES ('eseamons','answer90','Eric','Seamons','ericseamons90@gmail.com',3852087160,'4585 Carriage Lane Cedar Hills Utah 84062');


CREATE TABLE variables(id INT NOT NULL AUTO_INCREMENT, name varchar(100) UNIQUE, body varchar(60000), PRIMARY KEY (id));

INSERT INTO variables (name,body) VALUES ('about_us_description','<h1 style="text-align:center;color:green;">China Journey</h1>');


CREATE TABLE photos(photo_id INT NOT NULL AUTO_INCREMENT, photo_path varchar(200), photo_link varchar(200), PRIMARY KEY(photo_id));