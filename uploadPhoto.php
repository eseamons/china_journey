<!DOCTYPE html>
<html>
   <head>
      <title>Upload File</title>
   </head>
   <body>
        <?php
            include('functions.php');
            $target = "pictures/";
            $basename = basename( $_FILES['uploaded']['name']);
            $target = $target . $basename; 
            $ok=1;
            
            if (file_exists($target)) {
                echo "Sorry, the photo '".$basename."' already exists. ";
                $ok = 0;
            }
            
            
            if(move_uploaded_file($_FILES['uploaded']['tmp_name'], $target) && $ok == 1) 
            {
                echo "The photo ".$target. " has been uploaded";
                addPhoto($target);
            }
            else {
                echo "The file failed to upload";
            }
        ?>
      <br>
      <button onclick="window.location.href = 'https://china-journey-eseamons.c9.io/china_journey/photo_gallery.php' ">Return to Photo Gallery</button>
   </body>
</html>