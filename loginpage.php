<?php
  include('functions.php');
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Signin Template for Bootstrap</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <?php
		  echo getFavicon();
		?>
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="styles/login.css">
    <style>
      p {
        text-align: center;
      }
      .form-control:focus {
        border-color: #FFD700;
        box-shadow: inset 0 1px 1px rgba(255, 215, 0, 0.075), 0 0 8px rgba(255, 215, 0, 0.6);
      }
     /*http://charliepark.org/bootstrap_buttons/*/
    </style>
  </head>

  <body>
    
    
    
    
    
    
    <?php
      if(isset($_SESSION['username'])) {
        header('Location: https://china-journey-eseamons.c9.io/china_journey/');
      }
    
      if($_SESSION['authenticate'] == "failed") {
        echo '<p style="text-align:center;color:red;">Login Information invalid</p>';
        unset($_SESSION['authenticate']);
      }
      else if($_GET['msg'] == "notauthenticated") {
        echo '<p style="text-align:center;color:red;">You must first log in</p>';
      }
    ?>
    
    
    <div class="container">
      <form class="form-signin" name ="form" Method ="POST" Action ="authenticate.php">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label class="sr-only">Email address</label>
        <input name="inputusername" class="form-control" placeholder="username" required autofocus>
        <label class="sr-only">Password</label>
        <input type="password" name="inputpassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-danger" type="submit">Sign in</button>
      </form>
    </div> <!-- /container -->
    <a style="color:red" href="create_account.php"><p>No account? Create one here.</p></a>
    <!--#D9534F-->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>