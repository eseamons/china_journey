<?php
include('functions.php');
session_start();

if(isset($_SESSION['username'])) {
    $result = getAllUsers();
    $arr = array();
    while ($row = mysqli_fetch_assoc($result)) {
       $arr[] =  $row;
    }
        
    echo $json_response = json_encode($arr);
}
else {
    echo 'You are not authorized to access this page';
}
?>