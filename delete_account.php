<?php
    include('functions.php');
    session_start();
    if(!isset($_SESSION['username'])) {
        header('Location: https://china-journey-eseamons.c9.io/china_journey/loginpage.php?msg=notauthenticated');
    }
    else {
        $user_id = $_GET['uid'];
        $result = deleteUser($user_id);
        
        if($result !== FALSE) {
            $_SESSION['deleted'] = $user_id;
            header('Location: https://china-journey-eseamons.c9.io/china_journey/view_all_users.php');
        }
    }
              
    
?>