<?php
    include('functions.php');
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>All Users</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php
		  echo getFavicon();
		?>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	  <script src= "//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="styles/navbar.css">
      <link rel="stylesheet" type="text/css" href="styles/linen.css">
    </head>
    <body class = "linen" onload="messages();">
        
        <?php
            if(isset($_SESSION['username'])) {
                 echo getNavbar();
            }
            else {
                header('Location: https://china-journey-eseamons.c9.io/china_journey/loginpage.php?msg=notauthenticated');
            }
        ?>
        			<div style="margin-top:300px" ng-app="myApp" ng-controller="customersCtrl"> 
					
								<!--Panel that holds information for all users in site-->
								<div class ="row">
								<div class="col-md-10 col-md-offset-1">
									<div class="panel panel-danger">
									   <div class="panel-heading">
									      <h3 class="panel-title">All Users</h3>
									   </div>
									   <div class="panel-body">
									      All the users of the site are here, no matter what permissions they have on the site
									   </div>
									   <table class="table">
									      <th>User Id</th><th>Username</th><th>Password</th><th>First Name</th><th>Last Name</th><th>Email</th><th>Phone Number</th><th>Address</th><th>Edit</th>
									      <tr ng-repeat="u in users">
									      	<td>{{u.user_id}}</td><td>{{u.username}}</td><td>{{u.password}}</td><td>{{u.firstname}}</td><td>{{u.lastname}}</td><td>{{u.email}}</td><td>{{u.phone_number}}</td><td>{{u.address}}</td><td><a href="edit_user_info.php?uid={{u.user_id}}" style="color:black">
									      		<i class="fa fa-pencil"></i> edit</a></td>
									      </tr>
									   </table>
									</div>
								</div>
								</div>
								
					</div>
					
					<div class="row">
						<div class="col-md-offset-5">
							<a href="create_account.php"><button class="btn btn-md btn-danger"><i class="glyphicon glyphicon-user"></i>
Add New User</button></a>
						</div>
					</div>
					

    	<script>
    		/*global angular*/
    		var app = angular.module('myApp', []);
    		app.controller('customersCtrl', function($scope, $http) {
    		  $http.get("get_all_users.php")
    		  .success(function (data) {$scope.users = data});
    		});
    	</script>
    	
    	<?php
    		$added ='';
    		$deleted ='';
    		if(isset($_SESSION['deleted'])) {
    			$deleted = 'alert("The account has been successfully deleted");';
    				 unset($_SESSION['deleted']);
    		}
    		
    		if(isset($_SESSION['added'])) {
					$added = 'alert("You have successfully created a new account")';
				unset($_SESSION['added']);
			}
			
			$alert_html =  '
								<script>
									function messages() {
										'.$added.'
										'.$deleted.'
									}
								</script>
							   ';
			echo $alert_html;
    	?>
	
    </body>
</html>