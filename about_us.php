<?php
    include('functions.php');
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        
        <title>About Us</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php
		  echo getFavicon();
		?>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
      	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="styles/navbar.css">
        <link rel="stylesheet" type="text/css" href="styles/linen.css">
        
        <script type="text/javascript" src="script.js"></script> 
        <script type="text/javascript">
        /*global bkLib*/
            bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
        </script>
        
    </head>
    <body class="linen">
        <?php
            echo getNavbar();
            echo '<h1 class=" text-center" style="margin-top: 150px; font-size: 60px; color:red;">About China Journey</h1>';
            echo getVariable('about_us_description').'<br><br><br>';
            
            $body = getVariable("about_us_description");
        
            $editor_html = '
                                <form action="edit_about_us_description.php" method="post">
                                    <label>Editor for page:</label>
                                    <textarea name="area" rows="20" style="width: 100%;" >'.$body.
                                    
                                    
                                            
                                       
                           '
                                    </textarea>
                                    <INPUT TYPE = "Submit" Name = "Submit1" VALUE = "Update page">
                                </form>
                                <br>
            
                           ';
            if(isset($_SESSION['username'])) {
                echo $editor_html;
            }
        
        
        ?>
        
        
            
        
    </body>
</html>