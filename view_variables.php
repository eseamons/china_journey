<?php
    include('functions.php');
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Variables</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php
		  echo getFavicon();
		?>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="styles/navbar.css">
      <link rel="stylesheet" type="text/css" href="styles/linen.css">
    </head>
    <body class = "linen">
        
        <?php
            if(isset($_SESSION['username'])) {
                 echo getNavbar();
            }
            else {
                header('Location: https://china-journey-eseamons.c9.io/china_journey/loginpage.php?msg=notauthenticated');
            }
        ?>
	
	    <h1 class=" text-center" style="margin-top: 150px; font-size: 60px; color:black;">Site Variables</h1>
	    <?php
	        $result = getAllVariables();
	        $variables = array();
	        while ($row = mysqli_fetch_assoc($result)) {
               $table_rows .=  '<tr><td><a href="'.$row['id'].'" style="color:black">'.$row['name'].'</a></td></tr>';
            }
            
            
            echo  '
                    	<div class ="row">
            			<div class="col-md-4 col-md-offset-4">
            				<div class="panel panel-warning">
            				   <div class="panel-heading">
            				      <h3 class="panel-title">All Variables</h3>
            				   </div>
            				   <div class="panel-body">
            				      These are all the variables used on the site
            				   </div>
            				   <table class="table">
            				      <th>Name</th>
            				      '.$table_rows.'
            				   </table>
            				</div>
            			</div>
            			</div>
            
                   ';
                
    		
            
	    ?>
	    
	    <div class="row">
				<a href="add_new_variable.php"><button class="btn btn-md btn-danger center-block"><i class="fa fa-plus-square"></i>
Add New Variable</button></a>
		</div>
	    
    </body>
</html>