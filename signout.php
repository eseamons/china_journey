<?php
session_start();

if(isset($_SESSION['username'])) {
    session_unset();
    session_destroy();
    header('Location: https://china-journey-eseamons.c9.io/china_journey/');
}
else {
    header('Location: https://china-journey-eseamons.c9.io/china_journey/loginpage.php?msg=notauthenticated');
}
    
?>