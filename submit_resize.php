<?php
    include('functions.php');
    session_start();
    
    if(isset($_SESSION['username'])) {
        $image_html = $_POST['area'];
        $photo_id = $_GET['id'];
        echo editPhotoSize($photo_id, $image_html);
    
        header('Location: https://china-journey-eseamons.c9.io/china_journey/photo_gallery.php');
    }
    else {
        header('Location: https://china-journey-eseamons.c9.io/china_journey/loginpage.php?msg=notauthenticated');
    }

?>