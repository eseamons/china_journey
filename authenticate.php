<?php
    include('functions.php');
    session_start();
    
    $redirect_location = 'Location: https://china-journey-eseamons.c9.io/china_journey';
    
    $username = $_POST['inputusername'];
    $password = $_POST['inputpassword'];
    $row = getUserInfo($username,$password);
    
    if($row) {
        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password;
        $_SESSION['authenticate'] = TRUE;
    }
    else if (isset($_SESSION['username'])) {
        
    }
    else if ($username == '' && $password == '') {
        $redirect_location = 'Location: https://china-journey-eseamons.c9.io/china_journey/loginpage.php?msg=notauthenticated';
    }
    else {
        
        $redirect_location = 'Location: https://china-journey-eseamons.c9.io/china_journey/loginpage.php';
        $_SESSION['authenticate'] = "failed";
    }
    
    header($redirect_location);

?>