<!--http://stackoverflow.com/questions/15220704/how-to-detect-if-post-is-set-->
<!--http://stackoverflow.com/questions/30680138/mysql-syntax-error-check-manually-->
<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <body>
        <?php
            include('functions.php');
            if(count($_POST) == 0) {
                header('Location: https://china-journey-eseamons.c9.io/china_journey/create_account.php');
            }
            else {
                //set variables from post
                
                $username = $_POST['username'];
                $password = $_POST['password'];
                $firstname = $_POST['firstname'];
                $lastname = $_POST['lastname'];
                $email = $_POST['email'];
                $phone_number = $_POST['phonenumber'];
                $address = $_POST['address'];
                
                
                
                $result = insertUser($username,$password,$firstname,$lastname,$email,$phone_number,$address);
                
                
                
                if($result === FALSE) {
                    $_SESSION['failed'] = 'The account could not be created';
                    header('Location: https://china-journey-eseamons.c9.io/china_journey/create_account.php');
                }
                else {
                    $_SESSION['added'] = $firstname.' '.$lastname;
                    header('Location: https://china-journey-eseamons.c9.io/china_journey/view_all_users.php');
                }
            }  
            
        ?>
    </body>
</html>


