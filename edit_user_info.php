<?php
    include('functions.php');
    session_start();
    
    if(!isset($_SESSION['username'])) {
        header('Location: https://china-journey-eseamons.c9.io/china_journey/');
    }
?>
<!DOCTYPE html>
<html>
    <head>
	  
		<title>Edit User Info</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php
		  echo getFavicon();
		?>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
	  <link rel="stylesheet" type="text/css" href="styles/navbar.css">
	  <link rel="stylesheet" type="text/css" href="styles/linen.css">
	  <style>
		  	.form-control:focus {
	        border-color: rgba(255, 0, 0, 0.075);
	        box-shadow: inset 0 1px 1px rgba(236, 13, 13, 0.075), 0 0 8px rgba(236, 13, 13, 0.6);
	      }
	  </style>
	</head>
    <body class = "linen" onload="messages()">
      <?php
      	echo getNavbar();
      ?>
      <div style="margin-top:100px;" class="container">
          
			  <?php
                $user_id = $_GET['uid'];
                session_start();
                $_SESSION['uid'] = $user_id;
                $user = getUserById($user_id);
                echo '<h2>Account Information for '.$user['firstname'].' '.$user['lastname'].'</h2>';
                
                $form_html = 
                
                '
        			  <form role="form" name="account-creation" Method="post" Action="updateAccountInfo.php">
        			  	
        			  	<div class="row">
        				  	<div class="form-group  col-md-5">
        				      <label for="username">Username:</label>
        				      <input type="text" class="form-control" name="username" value="'.$user['username'].'" placeholder="Enter username" required>
        				    </div>
        				   </div>
        				   
        				 <div class="row"> 
        			    <div class="form-group col-md-5">
        			      <label for="pwd">Password:</label>
        			      <input type="text" class="form-control" name="password" value="'.$user['password'].'" placeholder="Enter password" required>
        			    </div>
        			   </div>
        			  
        		   	<div class="row"> 
        			  	<div class="form-group col-md-5">
        			      <label for="firstname">First Name:</label>
        			      <input type="text" class="form-control" name="firstname" value="'.$user['firstname'].'" placeholder="Enter firstname" required>
        			    </div>
        			  </div>
        			  
        			  <div class="row">  
        			    <div class="form-group col-md-5">
        			      <label for="lastname">Last Name:</label>
        			      <input type="text" class="form-control" name="lastname" value="'.$user['lastname'].'" placeholder="Enter lastname" required>
        			    </div>
        			  </div>
        			  
        			  <div class="row"> 
        			    <div class="form-group col-md-5">
        			      <label for="email">Email:</label>
        			      <input type="email" class="form-control" name="email" value="'.$user['email'].'" placeholder="Enter email" required>
        			    </div>
        			  </div>
        			  
        			  <div class="row"> 
        			    <div class="form-group col-md-5">
        			      <label for="phonenumber">Phone Number:</label>
        			      <input type="phonenumber" class="form-control" name="phonenumber" value="'.$user['phone_number'].'" placeholder="Enter phone number" required>
        			    </div>
        			  </div>
        			  
        			  <div class="row"> 
        			    <div class="form-group col-md-5">
        			      <label for="address">Address:</label>
        			      <input type="address" class="form-control" name="address" value="'.$user['address'].'" placeholder="Enter address" required>
        			    </div>
        			  </div>
        			    
        			    <div class="form-group">
        			      <button class="btn btn-md btn-danger" type="submit"><i class="fa fa-floppy-o"></i> Update Account</button>
        			    </div>
        			    
        			  </form>
        			  <a href="delete_account.php?uid='.$user_id.'" onclick="javascript:return confirm(\'Are you sure you want to delete the account for '.$user['firstname'].' '.$user['lastname'].'?\')"><button class="btn btn-md btn-danger"><i class="fa fa-trash"></i>
Delete Account</button></a>
        			</div>
        		';
        		echo $form_html;
        		//http://board.phpbuilder.com/showthread.php?10351229-RESOLVED-Create-Dialog-box-in-PHP
        		
        // 		$failed = '<script>
        // 		            function messages() {
        // 		                alert("The user could not be updated");
        // 		            }
        // 		        <script>
        // 		     ';
        		
        // 		if(isset($_SESSION['failed'])) {
        // 		    $failed = 'alert("The user could not be updated")';
        // 		}
        		
        // 	echo $failed;
        		     
        // 		     echo isset($_SESSION['failed']);
            ?>
  </body>
</html>