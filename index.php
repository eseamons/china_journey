<?php
  include('functions.php');
?>
<!DOCTYPE HTML>
<html>
	<head>
	  
		<title>China Journey</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php
		  echo getFavicon();
		?>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styles/navbar.css">
    <link rel="stylesheet" type="text/css" href="styles/mainbackground.css">
    
	</head>
	<body class = "backgroundImage">
    <?php
      echo getNavbar();
    ?>

    <h1 class=" text-center" style="margin-top: 200px; font-size: 60px">China Journey</h1>
    
    <h3 class="text-center">A tour of the middle kingdom that you will never forget</h4>
         <div class="container" id="columns-3">
          <!-- Example row of columns -->
          <div class="row">
            <div class="col-md-4">
              <h2>See new sites</h2>
              <p>Visit sites all over china with excellent tour guide commentary from experienced travels who are fluent in Manderin Chinese.</p>
            	<div class="visible-xs visible-sm" style="height: 50px;"></div>
    	</div>
            <div class="col-md-4">
              <h2>Gain new Knowledge</h2>
              <p>Gain new insites into chinese History, Culture, and famous important historical sites. Revel in your newfound knowledge of this amazing country.</p>
           		<div class="visible-xs visible-sm" style="height: 50px;"></div>
    	</div>
            <div class="col-md-4">
              <h2>Make New Friends</h2>
              <p>Make new friends as you travel to different parts of the country, both in our touring group and with natives that you meet during your travels</p>
            </div>
          </div>
          
          
      <?php
          if($_SESSION['authenticate'] == TRUE) {
    					$alert_html =  '
              									<script>
              										alert("You are now logged into the site");
              									</script>
            								 ';
                            
              echo $alert_html;
              unset($_SESSION['authenticate']);
          }
      ?>



</body>

</html>