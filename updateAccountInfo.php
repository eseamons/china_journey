<!--http://stackoverflow.com/questions/15220704/how-to-detect-if-post-is-set-->
<!--http://stackoverflow.com/questions/30680138/mysql-syntax-error-check-manually-->
<!DOCTYPE html>
<html>
    <body>
        <?php
            include('functions.php');
            session_start();
            if(count($_POST) == 0) {
                header('Location: https://china-journey-eseamons.c9.io/china_journey/create_account.php');
            }
            else {
                //set variables from post
                $user_id = $_SESSION['uid'];
                $username = $_POST['username'];
                $password = $_POST['password'];
                $firstname = $_POST['firstname'];
                $lastname = $_POST['lastname'];
                $email = $_POST['email'];
                $phone_number = $_POST['phonenumber'];
                $address = $_POST['address'];
                
                echo $username;
                
                $result = updateUser($user_id,$username,$password,$firstname,$lastname,$email,$phone_number,$address);
                
                
                if($result === FALSE) {
                    $_SESSION['failed'] = $user_id;
                    header('Location: https://china-journey-eseamons.c9.io/china_journey/edit_user_info.php?uid='.$user_id.'&msg=update_failed');
                }
                else {
                    header('Location: https://china-journey-eseamons.c9.io/china_journey/edit_user_info.php?uid='.$user_id.'&msg=update_successful');
                }
            }
            
        ?>
    </body>
</html>