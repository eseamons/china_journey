<?php
  session_start();
?>
<?php
function getConnection() { //get database connection
    //Connect to the database
    $host = "eseamons-china_journey-1681521";   //See Step 3 about how to get host name
    $user = "eseamons";                         //Your Cloud 9 username
    $pass = "";                                 //Remember, there is NO password!
    $db = "china_journey_db";                   //Your database name you want to connect to
    $port = 3306;                               //The port #. It is always 3306

    $connection = mysqli_connect($host, $user, $pass, $db, $port) or die(mysql_error());
    
    return $connection;
}

function getFavicon() {
  return '<link rel="shortcut icon" href="http://faviconist.com/icons/68f6d0a62f4b0b762e142cab9c21b2bb/favicon.ico"/>';
}

function getNavbar() {
  
  if(isset($_SESSION['username'])) {
    $login_button = '
                    <li>
                      <a href="signout.php">Log out</a>
                    </li>
                    ';
    $administration = '
                      <li class="dropdown">
                	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin<b class="caret"></b></a>
                	        <ul class="dropdown-menu">
                	          <li><a href="view_all_users.php">View Users</a></li>
                	          <li><a href="view_variables.php">View Variables</a></li>
                	        </ul>
                	    </li>
                      ';
                      
    $user = getUserInfo($_SESSION['username'],$_SESSION['password']);
    $name = $user['firstname'].' '.$user['lastname'];
    $logged_in_message = '<p style="color:white; font-size:17px;" class="navbar-text navbar-right">Signed in as '.$name.'</p>';
  }
  else {
    $login_button = '
                    <li>
                      <a href="loginpage.php">Log in</a>
                    </li>
                    ';
    $create_account = '
                        <li>
                          <a href="create_account.php">Create Account</a>
                        </li>
                      ';
  }

    $navbar = '
                <header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
                  <div class="container">
                    <div class="navbar-header">
                      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a href="/china_journey" class="navbar-brand" style="font-size: 22px;">China Journey</a>
                    </div>
                    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
                      <ul class="nav navbar-nav">
                        <li>
                          <a href="about_us.php">About Us</a>
                        </li>
                				<li class="dropdown">
                	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tour Info<b class="caret"></b></a>
                	        <ul class="dropdown-menu">
                	          <li><a href="#">Locations and Dates</a></li>
                	          <li><a href="#">Costs</a></li>
                	          <li><a href="#">Preparation</a></li>
                	          <li><a href="#">Testimonials</a></li>
                	        </ul>
                	      </li>
                        <li>
                          <a href="photo_gallery.php">Photo Gallery</a>
                        </li>
                        <li>
                          <a href="#">Contact Us</a>
                        </li>
                        
                        '.$logged_in_message.
                          $administration
                         .$login_button.
                         $create_account.
                        '
                        
                      </ul>
                    </nav>
                  </div>
                </header>
              ';
    echo $navbar;
}

function getUserInfo($username,$password) {
    $connection = getConnection();
    $query = "SELECT * FROM users WHERE username = '".$username."' AND password = '".$password."'";
    $result = mysqli_query($connection, $query);
    
    while ($row = mysqli_fetch_assoc($result)) {
        return $row;
    }
  
  return NULL;
}

function getAllUsers() {
    $connection = getConnection();
    $query = "SELECT * FROM users";
    $result = mysqli_query($connection, $query);
    
    return $result;
}

function insertUser($username,$password,$firstname,$lastname,$email,$phone_number,$address) {
  $connection = getConnection();
  $query = "INSERT INTO users (username,password,firstname,lastname,email,phone_number,address) VALUES ('".$username."','".$password."','".$firstname."','".$lastname."','".$email."','".$phone_number."','".$address."');";
  $result  = mysqli_query($connection, $query);
    
  return $result;
}

function updateUser($user_id,$username,$password,$firstname,$lastname,$email,$phone_number,$address) {
  $connection = getConnection();
  $query = '
              UPDATE users
              SET username =      "'.$username.'",
                  password =      "'.$password.'",
                  firstname =     "'.$firstname.'",
                  lastname =      "'.$lastname.'",
                  email =         "'.$email.'",
                  phone_number =  "'.$phone_number.'",
                  address  =      "'.$address.'"
                  WHERE user_id = '.$user_id.'
           ';
  $result  = mysqli_query($connection, $query);
    
  return $result;
}

function getUserById($user_id) {
  $connection = getConnection();
  $query = "SELECT * FROM users WHERE user_id = ".$user_id.";";
  $result  = mysqli_query($connection, $query);
  
  while ($row = mysqli_fetch_assoc($result)) {
         return $row;
  }
  
  return NULL;
}

function getVariable($name) {
  $connection = getConnection();
  $query = "SELECT * FROM variables WHERE name='".$name."';";
  $result = mysqli_query($connection, $query);
  
  while ($row = mysqli_fetch_assoc($result)) {
      return $row['body'];
  }
  return NULL;
}

function setVariable($body,$name) {
  $connection = getConnection();
  $query = "UPDATE variables SET body='".$body."' WHERE name='".$name."';";
  $result = mysqli_query($connection, $query);
  
  return $result;
}

function getAllVariables() {
  $connection = getConnection();
  $query = "SELECT * FROM variables;";
  $result = mysqli_query($connection, $query);
  
  return $result;
}

function createNewVariable($name,$body) {
  $connection = getConnection();
  $query = "INSERT INTO variables (name,body) VALUES ('".$name."','".$body."');";
  $result = mysqli_query($connection, $query);
  
  return $result;
}

function deleteUser($user_id) {
  $connection = getConnection();
  $query = "DELETE FROM users WHERE user_id = ".$user_id.";";
  $result = mysqli_query($connection, $query);
  
  return $result;
}

function getPhotoById($id) {
  $connection = getConnection();
  $query = "SELECT * FROM photos WHERE photo_id = ".$id.";";
  $result = mysqli_query($connection, $query);
  
  while ($row = mysqli_fetch_assoc($result)) {
      return $row['photo_link'];
  }
  return NULL;
}

function getAllPhotos() {
  $connection = getConnection();
  $query = "SELECT * FROM photos";
  $result = mysqli_query($connection, $query);
  
  return $result;
}

function addPhoto($path) {
  $connection = getConnection();
  $link = "<img src='".$path."' style='display: block; margin-left: auto; margin-right: auto;'></img><br><br><br><br><br>";
  $query = 'INSERT INTO photos (photo_path,photo_link) VALUES ("'.$path.'","'.$link.'");';
  $result = mysqli_query($connection, $query);
  
  return $result;
}

function editPhotoSize($id, $link) {
  $connection = getConnection();
  $query = "UPDATE photos SET photo_link = '".$link."' WHERE photo_id = ".$id.";";
  $result = mysqli_query($connection, $query);
  return $query;
}

?>