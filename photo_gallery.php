<?php
    include('functions.php');
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Photo Gallery</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php
		  echo getFavicon();
		?>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
      	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="styles/navbar.css">
        <link rel="stylesheet" type="text/css" href="styles/linen.css">
        
    </head>
    <body class="linen">
        <?php
            echo getNavbar();
        ?>
        
        <h1 class=" text-center" style="margin-top: 150px; font-size: 60px; color:black;">Photo Gallery</h1>
        
        <?php
        
            if(isset($_SESSION['username'])) {
                echo '    <div style="margin-left:50px;">
                          <form enctype="multipart/form-data" action="uploadPhoto.php" method="POST">
                          <b>Please choose a photo: </b><input name="uploaded" type="file"/><br/>
                          <input type="submit" value="Upload"/>
                          </form>
                          </div>
                     ';
            }
            echo '<br><br><br><br>';
            $result = getAllPhotos();
            while ($row = mysqli_fetch_assoc($result)) {
                $path = $row['photo_path'];
                $link = $row['photo_link'];
                $id = $row['photo_id'];
                
                if(isset($_SESSION['username'])) {
                    echo '<a href="picture_resize.php?id='.$id.'">'.$link.'</a>';
                }
                else {
                    echo $link;
                }
                
                
            }
            
                
        ?>
        
    </body>
</html>

<!--http://www.ajaxblender.com/howto-resize-image-proportionally-using-javascript.html-->