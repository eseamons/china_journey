<?php
  include('functions.php');
  session_start();
  //will change later when permissions are added
  //if(isset($_SESSION['username'])) {
  //	header("Location: https://china-journey-eseamons.c9.io/china_journey/");
  //}
?>
<!DOCTYPE HTML>
<html>
	<head>
	  
		<title>Create Account</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php
		  echo getFavicon();
		?>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	  <link rel="stylesheet" type="text/css" href="styles/navbar.css">
	  <link rel="stylesheet" type="text/css" href="styles/linen.css">
	  <style>
		  	.form-control:focus {
	        border-color: rgba(255, 0, 0, 0.075);
	        box-shadow: inset 0 1px 1px rgba(236, 13, 13, 0.075), 0 0 8px rgba(236, 13, 13, 0.6);
	      }
	  </style>
	</head>
	<body class = "linen" onload="messages();">
      <?php
      	echo getNavbar();
      ?>
      <div style="margin-top:100px;" class="container">
			  <h2>Create New Account</h2>
			  <form role="form" name="account-creation" Method="post" Action="add_account_to_site.php">
			  	
			  	<div class="row">
				  	<div class="form-group  col-md-5">
				      <label for="username">Username:</label>
				      <input type="text" class="form-control" name="username" placeholder="Enter username" required>
				    </div>
				   </div>
				   
				 <div class="row"> 
			    <div class="form-group col-md-5">
			      <label for="pwd">Password:</label>
			      <input type="text" class="form-control" name="password" placeholder="Enter password" required>
			    </div>
			   </div>
			  
		   	<div class="row"> 
			  	<div class="form-group col-md-5">
			      <label for="firstname">First Name:</label>
			      <input type="text" class="form-control" name="firstname" placeholder="Enter firstname" required>
			    </div>
			  </div>
			  
			  <div class="row">  
			    <div class="form-group col-md-5">
			      <label for="lastname">Last Name:</label>
			      <input type="text" class="form-control" name="lastname" placeholder="Enter lastname" required>
			    </div>
			  </div>
			  
			  <div class="row"> 
			    <div class="form-group col-md-5">
			      <label for="email">Email:</label>
			      <input type="email" class="form-control" name="email" placeholder="Enter email" required>
			    </div>
			  </div>
			  
			  <div class="row"> 
			    <div class="form-group col-md-5">
			      <label for="phonenumber">Phone Number:</label>
			      <input type="phonenumber" class="form-control" name="phonenumber" placeholder="Enter phone number" required>
			    </div>
			  </div>
			  
			  <div class="row"> 
			    <div class="form-group col-md-5">
			      <label for="address">Address:</label>
			      <input type="address" class="form-control" name="address" placeholder="Enter address" required>
			    </div>
			  </div>
			    
			    <div class="form-group">
			      <button class="btn btn-md btn-danger" type="submit">Create Account</button>
			    </div>
			    
			  </form>
			</div>
			
			<?php
	    		$failed ='';
	    		if(isset($_SESSION['failed'])) {
	    			$failed = 'alert("'.$_SESSION['failed'].'");';
	    				 unset($_SESSION['failed']);
	    		}
	    		
				
				$alert_html =  '
									<script>
										function messages() {
											'.$failed.'
										}
									</script>
								   ';
				echo $alert_html;
    		?>
		

  </body>

</html>