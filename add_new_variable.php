<?php
  include('functions.php');
  session_start();
  //will change later when permissions are added
  //if(isset($_SESSION['username'])) {
  //	header("Location: https://china-journey-eseamons.c9.io/china_journey/");
  //}
?>
<!DOCTYPE HTML>
<html>
	<head>
	  
		<title>Create Account</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php
		  echo getFavicon();
		?>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	  <link rel="stylesheet" type="text/css" href="styles/navbar.css">
	  <link rel="stylesheet" type="text/css" href="styles/linen.css">
	  
	  <script type="text/javascript" src="script.js"></script> 
	  <script type="text/javascript">
		/*global bkLib*/
		    bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
	  </script>
	  
	  <style>
		  	.form-control:focus {
	        border-color: rgba(255, 0, 0, 0.075);
	        box-shadow: inset 0 1px 1px rgba(236, 13, 13, 0.075), 0 0 8px rgba(236, 13, 13, 0.6);
	      }
	  </style>
	</head>
	<body class = "linen">
      <?php
      	echo getNavbar();
      ?>
      <div style="margin-top:100px;" class="container">
			  <h2>Create New Account</h2>
			  <form role="form" name="create_variable" Method="post" Action="create_variable.php">
			  	 
			  	<div class="row">
				  	<div class="form-group  col-md-5">
				      <label for="username">Variable Name:</label>
				      <input type="text" class="form-control" name="name" placeholder="Enter variable name" required>
				    </div>
				 </div>
				 
				 <?php
			    	$editor_html = '
                                    <label>Editor for page:</label>
                                    <textarea name="body" rows="20" style="width: 100%;">
                                    </textarea>
                                	<br>
                           		   ';
                           		   
                    echo $editor_html;
			    
			    ?>
				   
				
			    
			    <div class="form-group">
			      <button class="btn btn-md btn-danger" type="submit">Create Variable</button>
			    </div>
			    
			  </form>
			</div>
		

  </body>

</html>